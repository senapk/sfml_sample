#include <iostream>
#include <cmath>

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

using namespace std;
using namespace sf;

void draw_line(RenderWindow & win,
               float xa, float ya,
               float xb, float yb, float espessura){


    RectangleShape rect;
    rect.setOutlineThickness(espessura);
    rect.setOutlineColor(sf::Color::Red);
    rect.setPosition(xa, ya);

    float dist = sqrt((xb - xa)*(xb - xa) + (yb - ya) * (yb - ya));
    rect.setSize(Vector2f(dist, 0));

    auto sub = Vector2f(xa, ya) - Vector2f(xb, yb);
    rect.setRotation(atan(sub.y/sub.x)*(180/M_PI));

    win.draw(rect);
}

int main()
{
    sf::RectangleShape rect;
    rect.setFillColor(sf::Color::Blue);
    rect.setPosition(sf::Vector2f(30, 70));
    rect.setSize(Vector2f(200, 300));

    sf::CircleShape circulo;
    circulo.setRadius(100);
    circulo.setPosition(300, 100);
    circulo.setFillColor(sf::Color::Black);
    circulo.setOutlineColor(sf::Color::White);
    circulo.setOutlineThickness(2);

    sf::Font font;
    font.loadFromFile("../fonts/monof55.ttf");
    sf::Text text;
    text.setFont(font);
    text.setString("Digite Q, depois digite algo no terminal:");
    text.setPosition(50, 60);
    text.setCharacterSize(24);


    sf::RenderWindow win(sf::VideoMode(800, 600), "Janela");

    while(win.isOpen()){
        //tratamento eventos
        sf::Event event;
        while(win.pollEvent(event)){
            if(event.type == sf::Event::Closed)
                win.close();
            //if(event.type == sf::Event::KeyPressed)
              //  if(event.key == sf::Keyboard::A)
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q)){
            string texto;
            cout << "Digite o novo nome" << endl;
            cin >> texto;
            text.setString(texto);
        }
        //    win.close();
        //limpa
        win.clear();

        //pinta

        //win.draw(rect);
        win.draw(circulo);
        draw_line(win, 40, 50, 80, 30, 1.5);

        win.draw(rect);

        win.draw(text);
        //mostra
        win.display();
    }

    cout << "Hello World!" << endl;
    return 0;
}

